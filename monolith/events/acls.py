from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

def get_weather_info(city, state):

    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": [city, state, "ISO 3166-2:US"],
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        lat = content[0]["lat"]
        lon = content[0]["lon"]

    except (KeyError, IndexError):
        return {"lat": None, "lon": None}

    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        return {
            "weather": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
        }

    except (KeyError, IndexError):
        return {"weather": None, "temp": None}


def get_photo(city, state):

    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": "map of " + city + " " + state,
        "per_page": 1,
    }
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][10]["scr"]["original"]}

    except (KeyError, IndexError):
        return {"picture_url": None}
